import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';

@Injectable()
export class AnagramsService implements OnApplicationBootstrap {

  onApplicationBootstrap() {
    Logger.verbose(`Foo`);
  }

  isAnagram(x: string, y: string): boolean {

    if (!x || !y) {
      throw Error(`Input missed`);
    }

    if (typeof x !== 'string' || typeof y !== 'string') {
      throw Error(`Invalid input`);
    }

    if (x.length !== y.length) {
      return false;
    }

    const arrx = x.split('');
    const arry = y.split('');

    return false;
  }

}
