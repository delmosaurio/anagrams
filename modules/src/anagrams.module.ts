import { Module } from '@nestjs/common';
import { AnagramsService } from './service/anagrams.service';

@Module({
  providers: [
    AnagramsService,
  ],
  exports: [
    AnagramsService,
  ],
})
export class AnagramsModule {}
