import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { CheckCommand } from '../commands';
import { AnagramsModule  } from '@application/anagrams';

@Module({
  imports: [
    AnagramsModule,
  ],
  controllers: [AppController],
  providers: [
    CheckCommand
  ],
})
export class AppModule {}
