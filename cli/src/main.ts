import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import * as path from 'path';
import * as fs from 'fs';
import * as _ from 'lodash';

function isAnagram(x: string, y: string): boolean {

  if (!x || !y) {
    throw Error(`Input missed`);
  }

  if (typeof x !== 'string' || typeof y !== 'string') {
    throw Error(`Invalid input`);
  }

  if (x.length !== y.length) {
    return false;
  }

  const arrx = _.lowerCase(x).split('');
  const arry = _.lowerCase(y).split('');

  return _.sortBy(arrx).join('') === _.sortBy(arry).join('');
}

async function bootstrap() {
  // yargs(hideBin(process.argv))
  //   .command('check', '', (yargs) => {
  //     return yargs
  //       .positional('port', {
  //         describe: 'port to bind on',
  //         default: 5000
  //       })
  //   }, (argv) => {
  //     console.log(argv);
  //   })
  //   .option('verbose', {
  //     alias: 'v',
  //     type: 'boolean',
  //     description: 'Run with verbose logging'
  //   })
  //   .parse();

  const filename = path.resolve(__dirname, 'assets', 'Target_anagrams.txt');
  console.log(filename);

  const content = fs.readFileSync(filename, 'utf-8');
  const lines = content.split('\n');


  let counter = 0;
  lines.forEach(line => {
    const parts = line.split(/\s+:\s+/).filter(l => !!l);
    if (parts.length == 2) {
      const x = parts[0];
      const y = parts[1];
      const isA = isAnagram(x, y);

      if (isA) {
        counter++;
        console.log(x, y, 'Anagram');
      } else {
        console.log(x, y, 'Not Anagram');
      }

    }

  });

  console.log(`${counter} anagrams found`);

}



bootstrap();
