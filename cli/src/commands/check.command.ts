import { Command, CommandRunner, Option } from 'nest-commander';
import { Logger } from '@nestjs/common';

export interface CheckCommandOptions {
  filename?: string;
}

@Command({ name: 'check', description: '' })
export class CheckCommand extends CommandRunner {

  async run(
    passedParam: string[],
    options?: CheckCommandOptions,
  ): Promise<void> {
    Logger.log('ready to work!');
  }

}
